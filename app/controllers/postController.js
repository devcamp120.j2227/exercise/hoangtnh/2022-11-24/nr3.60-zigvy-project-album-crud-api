//import thư viện mongoose
const mongoose = require("mongoose");
//import post model
const postModel = require("../models/postModel");
const { post } = require("../routes/postRouter");
const userModel = require("../models/userModel");

//function create Post
const createPost = (request, response) =>{
    //B1: chuẩn bị dữ liệu
    let body = request.body;

    //B2: validate dữ liệu
    if(!body.title ){
        return response.status(400).json({
            status:"Bad request",
            message:"Title is not valid"
        })
    }
    if(!body.body){
        return response.status(400).json({
            status:"Bad request",
            message:"Body is not valid"
        })
    }

    //B3: thao tác với CSDL
    let newPost = {
        _id: mongoose.Types.ObjectId(),
        userId : mongoose.Types.ObjectId(),
        title: body.title,
        body: body.body
    }
    if(body.title !== undefined){
        newPost.title = body.title
    }
    if(body.body !== undefined){
        newPost.body = body.body
    }
    postModel.create(newPost, (error,data) =>{
        if(error){
            return response.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        }
        return response.status(201).json({
            status:"Create new post successfully",
            data: data
        })
    })
}
//function get all post
const getAllPost = (request, response) => {
    postModel.find((error,data) =>{
        if(error) {
            return response.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status:" Get all posts successfully",
            data: data
        })
    })
}
//function update post by id
const updatePost = (request, response) =>{
    //B1: chuẩn bị dữ liệu
    let postId = request.params.postId;
    let body = request.body

    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(postId)){
        return response.status(400).json({
            status:"Bad request",
            message:" Post id is not valid"
        })
    }
    if(!body.title ){
        return response.status(400).json({
            status:"Bad request",
            message:"Title is not valid"
        })
    }
    if(!body.body){
        return response.status(400).json({
            status:"Bad request",
            message:"Body is not valid"
        })
    }
    //B3: gọi model thao tác CSDL
    const postUpdate = {};
    if(body.title !== undefined){
        postUpdate.title = body.title
    }
    if(body.body !== undefined){
        postUpdate.body = body.body
    }
    postModel.findByIdAndUpdate(postId, postUpdate, (error, data) =>{
        if(error){
            return response.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        }
        return response.status(201).json({
            status:`Update post with id${postId} successfully`,
            data: data
        })
    })
}
//function get post by id
const getPostById = (request, response) => {
    //B1: chuẩn bị dữ liệu
    let postId = request.params.postId;
    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(postId)){
        return response.status(400).json({
            status:"Bad request",
            message:"Post id is not valid"
        })
    }
    //B3: gọi model chứa id thao tác với csdl
    postModel.findById(postId,(error, data) =>{
        if(error){
            return response.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status:`Get post by id ${postId} successfully`,
            data: data
        })
    })
}
//function delete post by id
const deletePost = (request, response) =>{
    //B1: chuẩn bị dữ liệu
    let postId = request.params.postId;

    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(postId )){
        return response.status(400).json({
            status:"Bad request",
            message: "Post id is not valid"
        })
    }
    //B3: thao tác với CSDL
    postModel.findByIdAndRemove(postId,(error, data) =>{
        if(error){
            return response.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status:`Deleted post by id ${postId} successfully`
        })
    })
}
//create post to user by user id
const createPostToUserId = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const userId = request.params.userId;
    const body = request.body;
    console.log(body)

    // B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "User ID không hợp lệ"
        })
    }

    if(!body.title ){
        return response.status(400).json({
            status:"Bad request",
            message:"Title is not valid"
        })
    }
    if(!body.body){
        return response.status(400).json({
            status:"Bad request",
            message:"Body is not valid"
        })
    }

    // B3: Thao tác với cơ sở dữ liệu
    const newPost = {
        _id: mongoose.Types.ObjectId(),
        userId: userId,
        title: body.title,
        body: body.body
    }

    postModel.create(newPost, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        // Thêm ID của post mới vào mảng posts của user đã chọn
        userModel.findByIdAndUpdate(userId, {
            $push: {
                posts: data._id
            }
        }, (err, updatedUser) => {
            if (err) {
                return response.status(500).json({
                    status: "Internal server error",
                    message: err.message
                })
            }

            return response.status(201).json({
                status: "Create Post Successfully",
                data: data
            })
        })
    })
}
//function get all post of user
const getAllPostOfUser = (request, response) =>{
    const userId = request.params.userId;

    // B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "User ID không hợp lệ"
        })
    }

    // B3: Thao tác với cơ sở dữ liệu
    userModel.findById(userId)
        .populate("posts")
        .exec((error, data) => {
            if (error) {
                return response.status(500).json({
                    status: "Internal server error",
                    message: error.message
                })
            }

            return response.status(200).json({
                status: "Get all posts of user successfully",
                data: data
            })
        })
}
//function get all post user id query
const getAllPostByQuery = (request, response) =>{
    const userId = request.query.userId;
    //nếu truyền vào user Id query
    if(userId){
        if(!mongoose.Types.ObjectId.isValid(userId)) {
            return response.status(400).json({
                status: "Error 400: Bad Request",
                message: "User ID is invalid"
            })
        }
        userModel.findById(userId)
            .populate("posts")
            .exec((error,data) =>{
            if(error){
                return response.status(500).json({
                    status:"Internal server error",
                    message: error.message
                })
            }
            return response.status(200).json({
                status:"Get posts by userId successfully",
                data: data.posts

            })
        })
    }
    //nếu không truyền vào user id
    else{
        postModel.find((error,data) =>{
            if(error){
                return response.status(500).json({
                    status:"Internal server error",
                    message: error.message
                })
            }
            return response.status(200).json({
                status:"Get all posts successfully",
                data: data

            })
        })
    }
    
}
module.exports = {
    createPost,
    //getAllPost,
    updatePost,
    getPostById,
    deletePost,
    createPostToUserId,
    getAllPostOfUser,
    getAllPostByQuery,
}