const mongoose = require("mongoose");
const albumModel = require("../models/albumModel");
const userModel = require("../models/userModel");

//function create album
const createAlbum = (request, response) =>{
    //B1: chuẩn bị dữ liệu
    let body = request.body;

    //B2: validate dữ liệu
    if(!body.title ){
        return response.status(400).json({
            status:"Bad request",
            message:"Title is not valid"
        })
    } 
   
    //B3: thao tác với CSDL
    let newAlbum = {
        _id: mongoose.Types.ObjectId(),
        userId : mongoose.Types.ObjectId(),
        title: body.title
    }
    if(body.title !== undefined){
        newAlbum.title = body.title
    }
    albumModel.create(newAlbum, (error,data) =>{
        if(error){
            return response.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        }
        return response.status(201).json({
            status:"Create new album successfully",
            data: data
        })
    })
}

//function get all album
const getAllAlbum = (request, response) => {
    albumModel.find((error,data) =>{
        if(error) {
            return response.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status:" Get all albums successfully",
            data: data
        })
    })
}
//function update album by id
const updateAlbum = (request, response) =>{
    //B1: chuẩn bị dữ liệu
    let albumId = request.params.albumId;
    let body = request.body

    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(albumId)){
        return response.status(400).json({
            status:"Bad request",
            message:" Album id is not valid"
        })
    }
    if(!body.title ){
        return response.status(400).json({
            status:"Bad request",
            message:"Title is not valid"
        })
    } 
    //B3: gọi model thao tác CSDL
    const albumUpdate = {};
    if(body.title !== undefined){
        albumUpdate.title = body.title
    }
    albumModel.findByIdAndUpdate(albumId, albumUpdate, (error, data) =>{
        if(error){
            return response.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        }
        return response.status(201).json({
            status:`Update album with id${albumId} successfully`,
            data: data
        })
    })
}
//function get album by id
const getAlbumById = (request, response) => {
    //B1: chuẩn bị dữ liệu
    let albumId = request.params.albumId;
    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(albumId)){
        return response.status(400).json({
            status:"Bad request",
            message:"Album id is not valid"
        })
    }
    //B3: gọi model chứa id thao tác với csdl
    albumModel.findById(albumId,(error, data) =>{
        if(error){
            return response.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status:`Get Album by id ${albumId} successfully`,
            data: data
        })
    })
}
//function delete album by id
const deleteAlbum = (request, response) =>{
    //B1: chuẩn bị dữ liệu
    let albumId = request.params.albumId;

    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(albumId )){
        return response.status(400).json({
            status:"Bad request",
            message: "Album id is not valid"
        })
    }
    //B3: thao tác với CSDL
    albumModel.findByIdAndRemove(albumId,(error, data) =>{
        if(error){
            return response.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status:`Deleted album by id ${albumId} successfully`
        })
    })
}
//function create album and push to user via albums obj
const createAlbumToUserId = (request, response) => {
    //B1 chuẩn bị dữ liệu
    const userId = request.params.userId;
    const body = request.body;
    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(userId)){
        return response.status(400).json({
            status:"Bad request",
            message: " User id is not valid"
        })
    }
    if(!body.title ){
        return response.status(400).json({
            status:"Bad request",
            message:"Title is not valid"
        })

    }
    //B3: thao tác với CSDL
    let newAlbum = {
        _id: mongoose.Types.ObjectId(),
        userId: userId,
        title: body.title
    }
   
    albumModel.create(newAlbum,(error, data) =>{
        if (error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        //Thêm Id của album mới vào mảng albums trong user
        userModel.findByIdAndUpdate(userId, {
            $push: {
                albums: data._id
            }
        }, (err,updateUser) =>{
            if(err){
                return response.status(500).json({
                    status: "Internal server error",
                    message: err.message
                })
            }
            return response.status(201).json({
                status:"Create album successfully",
                data: data
            })
        })
    })
}
//function get all albums of user
const getAllAlbumOfUser = (request, response) =>{
    const userId = request.params.userId;

    // B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "User ID không hợp lệ"
        })
    }

    // B3: Thao tác với cơ sở dữ liệu
    userModel.findById(userId)
        .populate("albums")
        .exec((error, data) => {
            if (error) {
                return response.status(500).json({
                    status: "Internal server error",
                    message: error.message
                })
            }

            return response.status(200).json({
                status: "Get all albums of user successfully",
                data: data
            })
        })
}
//function get all album user id using query
const getAllAlbumByQuery = (request, response) =>{
    const userId = request.query.userId;
    //nếu truyền vào user Id query
    if(userId){
        if(!mongoose.Types.ObjectId.isValid(userId)) {
            return response.status(400).json({
                status: "Error 400: Bad Request",
                message: "User ID is invalid"
            })
        }
        userModel.findById(userId)
            .populate("albums")
            .exec((error,data) =>{
            if(error){
                return response.status(500).json({
                    status:"Internal server error",
                    message: error.message
                })
            }
            return response.status(200).json({
                status:"Get albums by userId successfully",
                data: data.albums

            })
        })
    }
    //nếu không truyền vào user id
    else{
        albumModel.find((error,data) =>{
            if(error){
                return response.status(500).json({
                    status:"Internal server error",
                    message: error.message
                })
            }
            return response.status(200).json({
                status:"Get all albums successfully",
                data: data

            })
        })
    }
    
}
module.exports = {
    createAlbum,
    //getAllAlbum,
    updateAlbum,
    getAlbumById,
    deleteAlbum,
    createAlbumToUserId,
    getAllAlbumOfUser,
    getAllAlbumByQuery
}