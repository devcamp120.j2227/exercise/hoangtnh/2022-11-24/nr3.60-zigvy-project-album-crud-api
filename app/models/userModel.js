const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const geoSchema = new Schema({
    
    lat:{
        type: String,
        required: true
    },
    lng: {
        type: String,
        required: true
    }
}, { _id:false});

const addressSchema = new Schema({
    street:  {
        type: String,
        required: true
    },
    suite:  {
        type: String,
        required: true
    },
    city:  {
        type: String,
        required: true
    },
    zipcode:  {
        type: String,
        required: true
    },
    geo: geoSchema
}, { _id:false})

const companySchema = new Schema({
    name: {
        type: String,
        required: true
    },
    catchPhrase: {
        type: String,
        required: true
    },
    bs: {
        type: String,
        required: true
    }
}, { _id:false})

const userSchema = new Schema({
    _id: {
        type: mongoose.Types.ObjectId
    },
    name: {
        type: String,
        required: true
    },
    username: {
        type: String,
        required: true,
        unique: true
    },
    address: addressSchema,
    phone: {
        type: String,
        required: true
    },
    website: {
        type: String,
        require: true
    },
    company: companySchema,
    posts: [{
        type: mongoose.Types.ObjectId,
        ref:"post"
    }],
    albums: [{
        type: mongoose.Types.ObjectId,
        ref: "album"
    }],
    
});

module.exports = mongoose.model('user', userSchema);