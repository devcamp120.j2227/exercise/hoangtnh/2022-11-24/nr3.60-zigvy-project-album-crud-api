//khai báo thư viện mongoose
const mongoose = require("mongoose");
//khai báo schema
const Schema = mongoose.Schema;

const AlbumSchema = new Schema({
    _id: {
        type: mongoose.Types.ObjectId
    },
    userId: {
        type: String,
        ref: "user",
    },
    title: {
        type: String,
        require: true
    }
})
module.exports =mongoose.model("album", AlbumSchema)