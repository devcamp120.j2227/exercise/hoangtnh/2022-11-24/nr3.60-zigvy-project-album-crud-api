const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const postSchema = new Schema({
    _id: {
        type: mongoose.Types.ObjectId
    },
    userId : {
        type: String
    },
    title: {
        type: String,
        required: true
    },
    body: {
        type: String,
        required: true
    },
    comments:[{
        type: mongoose.Types.ObjectId,
        ref:"comment"
    }]
});

module.exports = mongoose.model("post", postSchema);